﻿using IcosExp.Models.General;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IcosExp.Models;
using IcosExp.ViewModels;

namespace IcosExp.Data
{
    public class ICOSContext : DbContext
    {
        public ICOSContext(DbContextOptions<ICOSContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // ...
            builder.Entity<BadmListItem>().ToTable("BADMList");
        }

        public DbSet<BadmListItem> BADMList { get; set; }

        public DbSet<TEAM> TEAM { get; set; }

        public DbSet<UTC> UTC { get; set; }

        public DbSet<LOCATION> LOCATION { get; set; }

        public DbSet<DataStorage> DataStorage { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<CLIM_AVG> CLIM_AVG { get; set; }

        public DbSet<LOCATIONViewModel> LOCATIONViewModel { get; set; }

        public DbSet<CLIM_AVGViewModel> CLIM_AVGViewModel { get; set; }
        public DbSet<EC> EC { get; set; }


    }
}
