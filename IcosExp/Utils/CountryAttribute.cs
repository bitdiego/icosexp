﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace IcosExp.Utils
{
    public class CountryAttribute : ValidationAttribute, IClientModelValidator
    {
        public string MyMessage { get; set; }

        protected override ValidationResult IsValid (object value, ValidationContext validationContext)
        {
            /*string country = value.ToString();

            if(!which(country)) // (country != "USA" && country != "UK" && country != "India")
            {
                return new ValidationResult("Invalid country. Valid values are USA, UK, and India.");
            }
            return ValidationResult.Success;

    */
            if (value == null)
            {
                return ValidationResult.Success;
            }
            bool valid = which(value.ToString());

            if (valid)
            {
                return ValidationResult.Success;
            }
            else
            {
                MyMessage = "Invalid country, you bastard. Valid values are USA, UK, and India.";
                return new ValidationResult(FormatErrorMessage(MyMessage));
            }

        }

        public void AddValidation (ClientModelValidationContext context)
        {
            context.Attributes.Add("data-val", "true");
            context.Attributes.Add("data-val-stoca", "Invalid country, you prick-head. Valid values are USA, UK, and India.");
        }

        private bool which(string country)
        {
            return (country == "USA" || country == "UK" || country == "India");
        }
    }
}
