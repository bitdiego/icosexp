﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Utils
{
    public class Globals
    {
        public enum Groups
        {
            GRP_HEADER=1,
            GRP_TWAM,
            GRP_LOCATION,
            GRP_UTC_OFFSET,
            GRP_LAND_OWNERSHIP,
            GRP_TOWER,
            GRP_CLIM_AVG,
            GRP_DM,
            GRP_PLOT,
            GRP_FLSM,
            GRP_SOSM,
            GRP_DHP,
            GRP_GAI,
            GRP_CEPT,
            GRP_BULKH,
            GRP_SPP,
            GRP_TREE,
            GRP_AGB,
            GRP_LITTER,
            GRP_ALLOM,
            GRP_WTD,
            GRP_D_SNOW,
            GRP_INST=2000,
            GRP_LOGGER,
            GRP_FILE,
            GRP_EC,
            GRP_ECSYS,
            GRP_BM,
            GRP_STO,
            GRP_CHEMICAL_DATA=2999,
            GRP_SPP_ASS,
            GRP_LAI,
            GRP_BIOMASS,
            GRP_HEIGHTC,
            GRP_SA,
            GRP_DBH,
            GRP_BASAL_AREA,
            GRP_TREES_NUM,
            GRP_ROOT_DEPTH,
            GRP_PHEN_EVENT_TYPE,
            GRP_IGBP,
            GRP_RESEARCH_TOPIC,
            GRP_SITE_FUNDING,
            GRP_URL,
            GRP_ACKNOWLEDGEMENT,
            GRP_FLUX_MEASUREMENTS,
            GRP_SITE_CHAR1,
            GRP_SITE_CHAR2,
            GRP_SITE_CHAR3,
            GRP_SITE_CHAR4,
            GRP_SITE_CHAR5,
            GRP_SOIL_CHEM,
            GRP_SOIL_STOCK,
            GRP_SOIL_TEX,
            GRP_PFCURVE,
            GRP_WTD_ASS,
            GRP_SWC,
            GRP_SOIL_WRB_GROUP,
            GRP_SOIL_ORDER,
            GRP_SOIL_CLASSIFICATION,
            GRP_SOIL_SERIES,
            GRP_SOIL_DEPTH,
            GRP_LITTER_ASS,
            GRP_SITE_DESC,
            GRP_SITE_NAME,
            GRP_SITE_ICOS_CLASS,
            GRP_CLIMATE_KOEPPEN,
            GRP_REFERENCE_PAPER,
            GRP_VAR_INFO=4000,
            GRP_VAR_AGG,
            GRP_INSTMAN=5000,
            GRP_INSTPAIR,
            GRP_ICOS
        }
        public enum CvIndexes
        {
            TEAM_ROLE = 11,
            TEAM_EXP=43,
            TEAM_PERC,
            TEAM_CONTR
        };
    }
}
