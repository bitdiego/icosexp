﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Utils
{
    public class IsoDateCompareAttribute : ValidationAttribute//, IClientModelValidator
    {
        public string MyMessage { get; set; }

        string otherValue="";
        
        public IsoDateCompareAttribute(string otherValue, string errorMessage)
            : base(errorMessage)
        {
            this.otherValue = otherValue;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ValidationResult validationResult = ValidationResult.Success;
            try
            {
                // Using reflection we can get a reference to the other date property, in this example the project start date
                var dateStartInfo = validationContext.ObjectType.GetProperty(this.otherValue);
                // Let's check that otherProperty is of type DateTime as we expect it to be
                if (dateStartInfo.PropertyType.Equals(/*new DateTime().GetType()*/otherValue.GetType()))
                {
                    string dateEnd = (string)value;
                    string dateStart = (string)dateStartInfo.GetValue(validationContext.ObjectInstance, null);

                    if(string.IsNullOrEmpty(dateStart) && string.IsNullOrEmpty(dateEnd))
                    {
                        validationResult = ValidationResult.Success;
                    }
                    else if(!string.IsNullOrEmpty(dateStart) && !string.IsNullOrEmpty(dateEnd))
                    // if the end date is lower than the start date, than the validationResult will be set to false and return
                    // a properly formatted error message
                    if (dateStart.CompareTo(dateEnd) > 0)
                    {
                        validationResult = new ValidationResult(ErrorMessageString);
                    }
                }
                else
                {
                    validationResult = new ValidationResult("An error occurred while validating the property. OtherProperty is not of type DateTime");
                }
            }
            catch (Exception ex)
            {
                // Do stuff, i.e. log the exception
                // Let it go through the upper levels, something bad happened
                throw ex;
            }

            return validationResult;
        }

       /* public void AddValidation(ClientModelValidationContext context)
        {
            string target = context.ModelMetadata.PropertyName.Replace("_", "-");
            string to = "data-val-comp-isodatecompare";
            context.Attributes.Add("data-val-comp", "true");
            context.Attributes.Add(to, "Invalid ISO date value");
        }
       */
        public bool CompareIsoDates(string dateStart, string dateEnd, bool both)
        {
            if (!both)
            {
                if(!String.IsNullOrEmpty(dateStart) && String.IsNullOrEmpty(dateEnd))
                {
                    return true;
                }
                if (String.IsNullOrEmpty(dateStart) && !String.IsNullOrEmpty(dateEnd))
                {
                    return false;
                }
            }
            else
            {
                if (String.IsNullOrEmpty(dateStart) && String.IsNullOrEmpty(dateEnd))
                {
                    return true;
                }
                else if (!String.IsNullOrEmpty(dateStart) && !String.IsNullOrEmpty(dateEnd))
                {
                    return String.Compare(dateEnd, dateStart)>0;
                }
            }
            return false;
        }
    }
}
