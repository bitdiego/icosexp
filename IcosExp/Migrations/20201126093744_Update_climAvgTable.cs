﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IcosExp.Migrations
{
    public partial class Update_climAvgTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UTC_OFFSET_COMMENT",
                table: "UTC",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 512,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "TEAM_MEMBER_LASTNAME",
                table: "TEAM",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "TEAM_MEMBER_FIRSTNAME",
                table: "TEAM",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "TEAM_MEMBER_EMAIL",
                table: "TEAM",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "TEAM_MEMBER_DATE",
                table: "TEAM",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "LOCATION_COMMENT",
                table: "LOCATION",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 10,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MAC_DATE",
                table: "CLIM_AVG",
                nullable: true,
                oldClrType: typeof(DateTime));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UTC_OFFSET_COMMENT",
                table: "UTC",
                maxLength: 512,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "TEAM_MEMBER_LASTNAME",
                table: "TEAM",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "TEAM_MEMBER_FIRSTNAME",
                table: "TEAM",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "TEAM_MEMBER_EMAIL",
                table: "TEAM",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "TEAM_MEMBER_DATE",
                table: "TEAM",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "LOCATION_COMMENT",
                table: "LOCATION",
                maxLength: 10,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "MAC_DATE",
                table: "CLIM_AVG",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
