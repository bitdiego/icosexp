﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IcosExp.Migrations
{
    public partial class EcModelTableAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            /*migrationBuilder.AlterColumn<string>(
                name: "LOCATION_COMMENT",
                table: "LOCATIONViewModel",
                maxLength: 512,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 10,
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "CLIM_AVGViewModel",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MAT = table.Column<decimal>(nullable: true),
                    MAP = table.Column<decimal>(nullable: true),
                    MAR = table.Column<decimal>(nullable: true),
                    MAC_YEARS = table.Column<decimal>(nullable: true),
                    MAS = table.Column<decimal>(nullable: true),
                    CLIMATE_KOEPPEN = table.Column<string>(nullable: true),
                    MAC_COMMENTS = table.Column<string>(nullable: true),
                    MAC_DATE = table.Column<string>(nullable: false),
                    DataStatus = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CLIM_AVGViewModel", x => x.Id);
                });
            */
            migrationBuilder.CreateTable(
                name: "EC",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DataStatus = table.Column<byte>(nullable: false),
                    InsertUserId = table.Column<long>(nullable: false),
                    InsertDate = table.Column<DateTime>(nullable: false),
                    DeleteUserId = table.Column<long>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    SiteId = table.Column<long>(nullable: false),
                    EC_MODEL = table.Column<string>(nullable: false),
                    EC_SN = table.Column<string>(nullable: false),
                    EC_TYPE = table.Column<string>(nullable: true),
                    EC_HEIGHT = table.Column<decimal>(nullable: true),
                    EC_EASTWARD_DIST = table.Column<decimal>(nullable: true),
                    EC_NORTHWARD_DIST = table.Column<decimal>(nullable: true),
                    EC_SAMPLING_INT = table.Column<decimal>(nullable: true),
                    EC_SA_HEAT = table.Column<string>(nullable: true),
                    EC_SA_OFFSET_N = table.Column<decimal>(nullable: true),
                    EC_SA_WIND_FORMAT = table.Column<string>(nullable: true),
                    EC_SA_GILL_ALIGN = table.Column<string>(nullable: true),
                    EC_SA_GILL_PCI = table.Column<string>(nullable: true),
                    EC_GA_FLOW_RATE = table.Column<decimal>(nullable: true),
                    EC_GA_LICOR_FM_SN = table.Column<string>(nullable: true),
                    EC_GA_LICOR_TP_SN = table.Column<string>(nullable: true),
                    EC_GA_LICOR_AIU_SN = table.Column<string>(nullable: true),
                    EC_GA_CAL_CO2_ZERO = table.Column<decimal>(nullable: true),
                    EC_GA_CAL_CO2_OFFSET = table.Column<decimal>(nullable: true),
                    EC_GA_CAL_CO2_REF = table.Column<decimal>(nullable: true),
                    EC_GA_CAL_H2O_ZERO = table.Column<decimal>(nullable: true),
                    EC_GA_CAL_TA = table.Column<decimal>(nullable: true),
                    EC_LOGGER = table.Column<int>(nullable: true),
                    EC_FILE = table.Column<int>(nullable: true),
                    EC_DATE = table.Column<string>(nullable: true),
                    EC_DATE_START = table.Column<string>(nullable: true),
                    EC_DATE_END = table.Column<string>(nullable: true),
                    EC_DATE_UNC = table.Column<decimal>(nullable: true),
                    EC_COMMENT = table.Column<string>(nullable: true),
                    EC_SA_NORTH_MAGDEC = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EC", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            /*migrationBuilder.DropTable(
                name: "CLIM_AVGViewModel");

            migrationBuilder.DropTable(
                name: "EC");

            migrationBuilder.AlterColumn<string>(
                name: "LOCATION_COMMENT",
                table: "LOCATIONViewModel",
                maxLength: 10,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 512,
                oldNullable: true);*/
        }
    }
}
