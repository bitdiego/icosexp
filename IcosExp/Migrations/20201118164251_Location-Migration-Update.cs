﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IcosExp.Migrations
{
    public partial class LocationMigrationUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "LOCATION_DATE",
                table: "LOCATIONViewModel",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<string>(
                name: "LOCATION_DATE",
                table: "LOCATION",
                nullable: true,
                oldClrType: typeof(DateTime));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "LOCATION_DATE",
                table: "LOCATIONViewModel",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "LOCATION_DATE",
                table: "LOCATION",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
