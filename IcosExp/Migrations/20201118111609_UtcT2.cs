﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IcosExp.Migrations
{
    public partial class UtcT2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UTC",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UTC_OFFSET = table.Column<decimal>(nullable: false),
                    UTC_OFFSET_DATE_START = table.Column<string>(nullable: true),
                    UTC_OFFSET_COMMENT = table.Column<string>(nullable: true),
                    DataStatus = table.Column<byte>(nullable: false),
                    InsertUserId = table.Column<long>(nullable: false),
                    InsertDate = table.Column<DateTime>(nullable: false),
                    DeleteUserId = table.Column<long>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    SiteId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UTC", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UTC");
        }
    }
}
