﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IcosExp.Migrations
{
    public partial class DataStorageT2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DataStorage",
                columns: table => new
                {
                    id_datastorage = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    siteID = table.Column<int>(nullable: false),
                    groupID = table.Column<int>(nullable: false),
                    groupName = table.Column<string>(nullable: true),
                    value = table.Column<string>(nullable: true),
                    DATE = table.Column<string>(nullable: true),
                    DATE_START = table.Column<string>(nullable: true),
                    DATE_END = table.Column<string>(nullable: true),
                    DATE_UNC = table.Column<string>(nullable: true),
                    COMMENT = table.Column<string>(nullable: true),
                    MEAS_PRECISION = table.Column<decimal>(nullable: true),
                    SPATIAL_VARIABILITY = table.Column<decimal>(nullable: true),
                    qual0 = table.Column<string>(nullable: true),
                    qual1 = table.Column<string>(nullable: true),
                    qual2 = table.Column<string>(nullable: true),
                    qual3 = table.Column<string>(nullable: true),
                    qual4 = table.Column<string>(nullable: true),
                    qual5 = table.Column<string>(nullable: true),
                    qual6 = table.Column<string>(nullable: true),
                    qual7 = table.Column<string>(nullable: true),
                    qual8 = table.Column<string>(nullable: true),
                    qual9 = table.Column<string>(nullable: true),
                    qual10 = table.Column<string>(nullable: true),
                    qual11 = table.Column<string>(nullable: true),
                    qual12 = table.Column<string>(nullable: true),
                    qual13 = table.Column<string>(nullable: true),
                    qual14 = table.Column<string>(nullable: true),
                    qual15 = table.Column<string>(nullable: true),
                    qual16 = table.Column<string>(nullable: true),
                    qual17 = table.Column<string>(nullable: true),
                    qual18 = table.Column<string>(nullable: true),
                    qual19 = table.Column<string>(nullable: true),
                    qual20 = table.Column<string>(nullable: true),
                    qual21 = table.Column<string>(nullable: true),
                    qual22 = table.Column<string>(nullable: true),
                    qual23 = table.Column<string>(nullable: true),
                    qual24 = table.Column<string>(nullable: true),
                    qual25 = table.Column<string>(nullable: true),
                    qual26 = table.Column<string>(nullable: true),
                    qual27 = table.Column<string>(nullable: true),
                    qual28 = table.Column<string>(nullable: true),
                    qual29 = table.Column<string>(nullable: true),
                    qual30 = table.Column<string>(nullable: true),
                    qual31 = table.Column<string>(nullable: true),
                    qual32 = table.Column<string>(nullable: true),
                    qual33 = table.Column<string>(nullable: true),
                    qual34 = table.Column<string>(nullable: true),
                    qual35 = table.Column<string>(nullable: true),
                    qual36 = table.Column<string>(nullable: true),
                    qual37 = table.Column<string>(nullable: true),
                    qual38 = table.Column<string>(nullable: true),
                    qual39 = table.Column<string>(nullable: true),
                    qual40 = table.Column<string>(nullable: true),
                    qual41 = table.Column<string>(nullable: true),
                    qual42 = table.Column<string>(nullable: true),
                    qual43 = table.Column<string>(nullable: true),
                    qual44 = table.Column<string>(nullable: true),
                    qual45 = table.Column<string>(nullable: true),
                    qual46 = table.Column<string>(nullable: true),
                    qual47 = table.Column<string>(nullable: true),
                    qual48 = table.Column<string>(nullable: true),
                    qual49 = table.Column<string>(nullable: true),
                    qual50 = table.Column<string>(nullable: true),
                    insertDate = table.Column<DateTime>(nullable: true),
                    insertUserId = table.Column<int>(nullable: false),
                    deleteUserId = table.Column<int>(nullable: true),
                    deletedDate = table.Column<DateTime>(nullable: true),
                    dataStatus = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataStorage", x => x.id_datastorage);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DataStorage");
        }
    }
}
