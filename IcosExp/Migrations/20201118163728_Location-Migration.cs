﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IcosExp.Migrations
{
    public partial class LocationMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UTC_OFFSET_COMMENT",
                table: "UTC",
                maxLength: 512,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "LOCATION",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LOCATION_LAT = table.Column<decimal>(nullable: false),
                    LOCATION_LONG = table.Column<decimal>(nullable: false),
                    LOCATION_ELEV = table.Column<decimal>(nullable: false),
                    LOCATION_DATE = table.Column<DateTime>(nullable: false),
                    LOCATION_COMMENT = table.Column<string>(maxLength: 10, nullable: true),
                    DataStatus = table.Column<byte>(nullable: false),
                    InsertUserId = table.Column<long>(nullable: false),
                    InsertDate = table.Column<DateTime>(nullable: false),
                    DeleteUserId = table.Column<long>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    SiteId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LOCATION", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LOCATIONViewModel",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LOCATION_LAT = table.Column<decimal>(nullable: false),
                    LOCATION_LONG = table.Column<decimal>(nullable: false),
                    LOCATION_ELEV = table.Column<decimal>(nullable: false),
                    LOCATION_DATE = table.Column<DateTime>(nullable: false),
                    LOCATION_COMMENT = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LOCATIONViewModel", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LOCATION");

            migrationBuilder.DropTable(
                name: "LOCATIONViewModel");

            migrationBuilder.AlterColumn<string>(
                name: "UTC_OFFSET_COMMENT",
                table: "UTC",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 512,
                oldNullable: true);
        }
    }
}
