﻿// <auto-generated />
using System;
using IcosExp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace IcosExp.Migrations
{
    [DbContext(typeof(ICOSContext))]
    [Migration("20201118164251_Location-Migration-Update")]
    partial class LocationMigrationUpdate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("IcosExp.Models.Customer", b =>
                {
                    b.Property<string>("CustomerID")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(5);

                    b.Property<string>("CompanyName")
                        .IsRequired()
                        .HasMaxLength(40);

                    b.Property<string>("ContactName")
                        .IsRequired()
                        .HasMaxLength(40);

                    b.Property<string>("Country")
                        .IsRequired();

                    b.HasKey("CustomerID");

                    b.ToTable("Customers");
                });

            modelBuilder.Entity("IcosExp.Models.DataStorage", b =>
                {
                    b.Property<int>("id_datastorage")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("COMMENT");

                    b.Property<string>("DATE");

                    b.Property<string>("DATE_END");

                    b.Property<string>("DATE_START");

                    b.Property<string>("DATE_UNC");

                    b.Property<decimal?>("MEAS_PRECISION");

                    b.Property<decimal?>("SPATIAL_VARIABILITY");

                    b.Property<short?>("dataStatus");

                    b.Property<int?>("deleteUserId");

                    b.Property<DateTime?>("deletedDate");

                    b.Property<int>("groupID");

                    b.Property<string>("groupName");

                    b.Property<DateTime?>("insertDate");

                    b.Property<int>("insertUserId");

                    b.Property<string>("qual0");

                    b.Property<string>("qual1");

                    b.Property<string>("qual10");

                    b.Property<string>("qual11");

                    b.Property<string>("qual12");

                    b.Property<string>("qual13");

                    b.Property<string>("qual14");

                    b.Property<string>("qual15");

                    b.Property<string>("qual16");

                    b.Property<string>("qual17");

                    b.Property<string>("qual18");

                    b.Property<string>("qual19");

                    b.Property<string>("qual2");

                    b.Property<string>("qual20");

                    b.Property<string>("qual21");

                    b.Property<string>("qual22");

                    b.Property<string>("qual23");

                    b.Property<string>("qual24");

                    b.Property<string>("qual25");

                    b.Property<string>("qual26");

                    b.Property<string>("qual27");

                    b.Property<string>("qual28");

                    b.Property<string>("qual29");

                    b.Property<string>("qual3");

                    b.Property<string>("qual30");

                    b.Property<string>("qual31");

                    b.Property<string>("qual32");

                    b.Property<string>("qual33");

                    b.Property<string>("qual34");

                    b.Property<string>("qual35");

                    b.Property<string>("qual36");

                    b.Property<string>("qual37");

                    b.Property<string>("qual38");

                    b.Property<string>("qual39");

                    b.Property<string>("qual4");

                    b.Property<string>("qual40");

                    b.Property<string>("qual41");

                    b.Property<string>("qual42");

                    b.Property<string>("qual43");

                    b.Property<string>("qual44");

                    b.Property<string>("qual45");

                    b.Property<string>("qual46");

                    b.Property<string>("qual47");

                    b.Property<string>("qual48");

                    b.Property<string>("qual49");

                    b.Property<string>("qual5");

                    b.Property<string>("qual50");

                    b.Property<string>("qual6");

                    b.Property<string>("qual7");

                    b.Property<string>("qual8");

                    b.Property<string>("qual9");

                    b.Property<int>("siteID");

                    b.Property<string>("value");

                    b.HasKey("id_datastorage");

                    b.ToTable("DataStorage");
                });

            modelBuilder.Entity("IcosExp.Models.General.BadmListItem", b =>
                {
                    b.Property<int>("id_badmlist")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("cv_index");

                    b.Property<string>("description");

                    b.Property<string>("shortname");

                    b.Property<string>("vocabulary");

                    b.HasKey("id_badmlist");

                    b.ToTable("BADMList");
                });

            modelBuilder.Entity("IcosExp.Models.General.LOCATION", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<byte>("DataStatus");

                    b.Property<long>("DeleteUserId");

                    b.Property<DateTime>("DeletedDate");

                    b.Property<DateTime>("InsertDate");

                    b.Property<long>("InsertUserId");

                    b.Property<string>("LOCATION_COMMENT")
                        .HasMaxLength(10);

                    b.Property<string>("LOCATION_DATE");

                    b.Property<decimal>("LOCATION_ELEV");

                    b.Property<decimal>("LOCATION_LAT");

                    b.Property<decimal>("LOCATION_LONG");

                    b.Property<long>("SiteId");

                    b.HasKey("Id");

                    b.ToTable("LOCATION");
                });

            modelBuilder.Entity("IcosExp.Models.General.TEAM", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<byte>("DataStatus");

                    b.Property<long>("DeleteUserId");

                    b.Property<DateTime>("DeletedDate");

                    b.Property<DateTime>("InsertDate");

                    b.Property<long>("InsertUserId");

                    b.Property<long>("SiteId");

                    b.Property<string>("TEAM_MEMBER_COMMENT");

                    b.Property<string>("TEAM_MEMBER_CONTRACT");

                    b.Property<string>("TEAM_MEMBER_DATE")
                        .IsRequired();

                    b.Property<string>("TEAM_MEMBER_EMAIL")
                        .IsRequired();

                    b.Property<string>("TEAM_MEMBER_FIRSTNAME")
                        .IsRequired();

                    b.Property<string>("TEAM_MEMBER_INSTITUTION");

                    b.Property<string>("TEAM_MEMBER_INST_CITY");

                    b.Property<string>("TEAM_MEMBER_INST_COUNTRY");

                    b.Property<string>("TEAM_MEMBER_INST_POSTCODE");

                    b.Property<string>("TEAM_MEMBER_INST_STREET");

                    b.Property<string>("TEAM_MEMBER_LASTNAME")
                        .IsRequired();

                    b.Property<string>("TEAM_MEMBER_MAIN_EXPERT");

                    b.Property<string>("TEAM_MEMBER_ORCID");

                    b.Property<string>("TEAM_MEMBER_PERC_ICOS");

                    b.Property<string>("TEAM_MEMBER_PHONE");

                    b.Property<string>("TEAM_MEMBER_ROLE");

                    b.Property<string>("TEAM_MEMBER_SOCIALMEDIA");

                    b.Property<string>("TEAM_MEMBER_TITLE");

                    b.Property<string>("TEAM_MEMBER_WORKEND");

                    b.HasKey("Id");

                    b.ToTable("TEAM");
                });

            modelBuilder.Entity("IcosExp.Models.General.UTC", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<byte>("DataStatus");

                    b.Property<long>("DeleteUserId");

                    b.Property<DateTime>("DeletedDate");

                    b.Property<DateTime>("InsertDate");

                    b.Property<long>("InsertUserId");

                    b.Property<long>("SiteId");

                    b.Property<decimal>("UTC_OFFSET");

                    b.Property<string>("UTC_OFFSET_COMMENT")
                        .HasMaxLength(512);

                    b.Property<string>("UTC_OFFSET_DATE_START");

                    b.HasKey("Id");

                    b.ToTable("UTC");
                });

            modelBuilder.Entity("IcosExp.ViewModels.LOCATIONViewModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("LOCATION_COMMENT")
                        .HasMaxLength(10);

                    b.Property<string>("LOCATION_DATE");

                    b.Property<decimal>("LOCATION_ELEV");

                    b.Property<decimal>("LOCATION_LAT");

                    b.Property<decimal>("LOCATION_LONG");

                    b.HasKey("Id");

                    b.ToTable("LOCATIONViewModel");
                });
#pragma warning restore 612, 618
        }
    }
}
