﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IcosExp.Migrations
{
    public partial class Team2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           /* migrationBuilder.DropTable(
                name: "GRP_TEAM");
           */
            migrationBuilder.CreateTable(
                name: "TEAM",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TEAM_MEMBER_FIRSTNAME = table.Column<string>(nullable: false),
                    TEAM_MEMBER_LASTNAME = table.Column<string>(nullable: false),
                    TEAM_MEMBER_TITLE = table.Column<string>(nullable: true),
                    TEAM_MEMBER_ROLE = table.Column<string>(nullable: true),
                    TEAM_MEMBER_MAIN_EXPERT = table.Column<string>(nullable: true),
                    TEAM_MEMBER_PERC_ICOS = table.Column<string>(nullable: true),
                    TEAM_MEMBER_CONTRACT = table.Column<string>(nullable: true),
                    TEAM_MEMBER_EMAIL = table.Column<string>(nullable: false),
                    TEAM_MEMBER_ORCID = table.Column<string>(nullable: true),
                    TEAM_MEMBER_SOCIALMEDIA = table.Column<string>(nullable: true),
                    TEAM_MEMBER_PHONE = table.Column<string>(nullable: true),
                    TEAM_MEMBER_INSTITUTION = table.Column<string>(nullable: true),
                    TEAM_MEMBER_INST_STREET = table.Column<string>(nullable: true),
                    TEAM_MEMBER_INST_POSTCODE = table.Column<string>(nullable: true),
                    TEAM_MEMBER_INST_CITY = table.Column<string>(nullable: true),
                    TEAM_MEMBER_INST_COUNTRY = table.Column<string>(nullable: true),
                    TEAM_MEMBER_WORKEND = table.Column<string>(nullable: true),
                    TEAM_MEMBER_COMMENT = table.Column<string>(nullable: true),
                    TEAM_MEMBER_DATE = table.Column<string>(nullable: false),
                    DataStatus = table.Column<byte>(nullable: false),
                    InsertUserId = table.Column<long>(nullable: false),
                    InsertDate = table.Column<DateTime>(nullable: false),
                    DeleteUserId = table.Column<long>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    SiteId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TEAM", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TEAM");

            migrationBuilder.CreateTable(
                name: "GRP_TEAM",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DataStatus = table.Column<byte>(nullable: false),
                    DeleteUserId = table.Column<long>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    InsertDate = table.Column<DateTime>(nullable: false),
                    InsertUserId = table.Column<long>(nullable: false),
                    SiteId = table.Column<long>(nullable: false),
                    TEAM_MEMBER_COMMENT = table.Column<string>(nullable: true),
                    TEAM_MEMBER_CONTRACT = table.Column<string>(nullable: true),
                    TEAM_MEMBER_DATE = table.Column<string>(nullable: false),
                    TEAM_MEMBER_EMAIL = table.Column<string>(nullable: false),
                    TEAM_MEMBER_FIRSTNAME = table.Column<string>(nullable: false),
                    TEAM_MEMBER_INSTITUTION = table.Column<string>(nullable: true),
                    TEAM_MEMBER_INST_CITY = table.Column<string>(nullable: true),
                    TEAM_MEMBER_INST_COUNTRY = table.Column<string>(nullable: true),
                    TEAM_MEMBER_INST_POSTCODE = table.Column<string>(nullable: true),
                    TEAM_MEMBER_INST_STREET = table.Column<string>(nullable: true),
                    TEAM_MEMBER_LASTNAME = table.Column<string>(nullable: false),
                    TEAM_MEMBER_MAIN_EXPERT = table.Column<string>(nullable: true),
                    TEAM_MEMBER_ORCID = table.Column<string>(nullable: true),
                    TEAM_MEMBER_PERC_ICOS = table.Column<string>(nullable: true),
                    TEAM_MEMBER_PHONE = table.Column<string>(nullable: true),
                    TEAM_MEMBER_ROLE = table.Column<string>(nullable: true),
                    TEAM_MEMBER_SOCIALMEDIA = table.Column<string>(nullable: true),
                    TEAM_MEMBER_TITLE = table.Column<string>(nullable: true),
                    TEAM_MEMBER_WORKEND = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GRP_TEAM", x => x.Id);
                });
        }
    }
}
