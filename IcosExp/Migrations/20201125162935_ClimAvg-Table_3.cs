﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IcosExp.Migrations
{
    public partial class ClimAvgTable_3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CLIM_AVG",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MAT = table.Column<decimal>(nullable: false),
                    MAP = table.Column<decimal>(nullable: false),
                    MAR = table.Column<decimal>(nullable: false),
                    MAC_YEARS = table.Column<decimal>(nullable: false),
                    MAS = table.Column<decimal>(nullable: false),
                    CLIMATE_KOEPPEN = table.Column<string>(nullable: true),
                    MAC_COMMENTS = table.Column<string>(nullable: true),
                    MAC_DATE = table.Column<DateTime>(nullable: false),
                    DataStatus = table.Column<byte>(nullable: false),
                    InsertUserId = table.Column<long>(nullable: false),
                    InsertDate = table.Column<DateTime>(nullable: false),
                    DeleteUserId = table.Column<long>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    SiteId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CLIM_AVG", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CLIM_AVG");
        }
    }
}
