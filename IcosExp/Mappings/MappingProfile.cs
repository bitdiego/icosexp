﻿using AutoMapper;
using IcosExp.Models.General;
using IcosExp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Mappings
{
    public abstract class MappingProfile<TSource, TDest> : Profile 
    {
        //private MapperConfiguration _cfg;
        public MappingProfile()
        {
            CreateMap<TSource, TDest>().IgnoreAllSourcePropertiesWithAnInaccessibleSetter();
        }
        
    }

    public class TeamVMMappingProfile : MappingProfile<TEAM, TEAMViewModel> { }

    public class TeamMappingProfile : MappingProfile<TEAMViewModel, TEAM> { }

    public class LocationVMMappingProfile : MappingProfile<LOCATION, LOCATIONViewModel> { }

    public class LocationMappingProfile : MappingProfile<LOCATIONViewModel, LOCATION> { }

    public class ClimateAvgVMMappingProfile : MappingProfile<CLIM_AVG, CLIM_AVGViewModel> { }

    public class ClimateAvgMappingProfile : MappingProfile<CLIM_AVGViewModel, CLIM_AVG> { }

}
