﻿using IcosExp.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.ViewModels
{
    public class LOCATIONViewModel
    {
        public int Id { get; set; }

        public int DataStatus { get; set; }

        [Required]
       // [Range(-90.0, 90.0, ErrorMessage = "Latitude must range between -90 to +90 degrees")]
        public decimal LOCATION_LAT { get; set; }

        [Required]
       // [Range(-180.0, 180.0, ErrorMessage = "Longitude must range between -180 to +180")]
        public decimal LOCATION_LONG { get; set; }

        [Range(0.0, 9999.0, ErrorMessage = "Elevation must have a numeric value")]
        public decimal LOCATION_ELEV { get; set; }

        [IsoDate]
        public string LOCATION_DATE { get; set; }

        [StringLength(512, ErrorMessage ="Warning: max numbers of allowed characters is 512")]
        public string LOCATION_COMMENT { get; set; }
    }
}
