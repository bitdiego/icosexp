﻿using IcosExp.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.ViewModels
{
    public class CLIM_AVGViewModel
    {
        public int Id { get; set; }

        public decimal? MAT { get; set; }

        public decimal? MAP { get; set; }

        public decimal? MAR { get; set; }

        public decimal? MAC_YEARS { get; set; }

        public decimal? MAS { get; set; }

        public string CLIMATE_KOEPPEN { get; set; }

        public string MAC_COMMENTS { get; set; }

        [Required]
        [IsoDate]
        public string MAC_DATE { get; set; }

        public Byte DataStatus { get; set; }
    }
}
