﻿using IcosExp.Services;
using IcosExp.Utils;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.ViewModels
{
    public class TEAMViewModel
    {

        public TEAMViewModel()
        {
        }

        [Required]
        public string TEAM_MEMBER_FIRSTNAME { get; set; }

        [Required]
        public string TEAM_MEMBER_LASTNAME { get; set; }

        public string TEAM_MEMBER_TITLE { get; set; }

        public string TEAM_MEMBER_ROLE { get; set; }

        public string TEAM_MEMBER_MAIN_EXPERT { get; set; }

        public string TEAM_MEMBER_PERC_ICOS { get; set; }

        public string TEAM_MEMBER_CONTRACT { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "The email format is not valid")]
        public string TEAM_MEMBER_EMAIL { get; set; }

        public string TEAM_MEMBER_ORCID { get; set; }

        public string TEAM_MEMBER_SOCIALMEDIA { get; set; }

        [RegularExpression(@"^\+[0-9]+$", ErrorMessage = "Invalid phone number")]
        public string TEAM_MEMBER_PHONE { get; set; }

        public string TEAM_MEMBER_INSTITUTION { get; set; }

        public string TEAM_MEMBER_INST_STREET { get; set; }

        public string TEAM_MEMBER_INST_POSTCODE { get; set; }

        public string TEAM_MEMBER_INST_CITY { get; set; }

        public string TEAM_MEMBER_INST_COUNTRY { get; set; }

        [IsoDate]
        [IsoDateCompare("TEAM_MEMBER_DATE", "TEAM_MEMBER_WORKEND must be greater than the start date of the project")]
        public String TEAM_MEMBER_WORKEND { get; set; }

        public string TEAM_MEMBER_COMMENT { get; set; }

        //[Required]
        [IsoDate]
        public String TEAM_MEMBER_DATE { get; set; }

        public int Id { get; set; }

        //Nav Properties
        public List<SelectListItem> TeamRoles { get; set; }

        public List<SelectListItem> TeamPerc { get; set; }

        public List<SelectListItem> TeamExpert { get; set; }

        public List<SelectListItem> TeamContract { get; set; }

    }
}
