﻿using IcosExp.Utils;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.ViewModels
{
    public class ECViewModel
    {
        public ECViewModel()
        {
			EcTypes = new List<SelectListItem>();
			EcTypes.Add(new SelectListItem("Installation", "Installation"));
			EcTypes.Add(new SelectListItem("Maintenance", "Maintenance"));
			EcTypes.Add(new SelectListItem("Removal", "Removal"));
		}
        public int Id { get; set; }

        [Required]
		public string EC_MODEL { get; set; }
		
		[Required]
		public string EC_SN { get; set; }
		
		[Required]
		public string EC_TYPE { get; set; }

		public decimal? EC_HEIGHT { get; set; }

		public decimal? EC_EASTWARD_DIST { get; set; }

		public decimal? EC_NORTHWARD_DIST { get; set; }

		public decimal? EC_SAMPLING_INT { get; set; }

		public string EC_SA_HEAT { get; set; }

		public decimal? EC_SA_OFFSET_N { get; set; }

		public string EC_SA_WIND_FORMAT { get; set; }

		public string EC_SA_GILL_ALIGN { get; set; }

		public string EC_SA_GILL_PCI { get; set; }

		public decimal? EC_GA_FLOW_RATE { get; set; }

		public string EC_GA_LICOR_FM_SN { get; set; }

		public string EC_GA_LICOR_TP_SN { get; set; }

		public string EC_GA_LICOR_AIU_SN { get; set; }

		public decimal? EC_GA_CAL_CO2_ZERO { get; set; }

		public decimal? EC_GA_CAL_CO2_OFFSET { get; set; }

		public decimal? EC_GA_CAL_CO2_REF { get; set; }

		public decimal? EC_GA_CAL_H2O_ZERO { get; set; }

		public decimal? EC_GA_CAL_TA { get; set; }

		public int? EC_LOGGER { get; set; }

		public int? EC_FILE { get; set; }

		public string EC_DATE { get; set; }

		public string EC_DATE_START { get; set; }

		public string EC_DATE_END { get; set; }

		public decimal? EC_DATE_UNC { get; set; }

		public string EC_COMMENT { get; set; }

		public decimal? EC_SA_NORTH_MAGDEC { get; set; }

		/************************************/
		public List<SelectListItem> EcTypes { get; set; }
	}
}
