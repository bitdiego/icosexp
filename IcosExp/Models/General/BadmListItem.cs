﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Models.General
{
    public class BadmListItem
    {
        [Key]
        public int Id { get; set; }

        public string vocabulary { get; set; }

        public string shortname { get; set; }

        public string description { get; set; }

        public int cv_index { get; set; }
    }
}
