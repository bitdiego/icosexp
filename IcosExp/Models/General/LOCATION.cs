﻿using IcosExp.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using IcosExp.Utils;

namespace IcosExp.Models.General
{
    public class LOCATION : BaseClass
    {
        // public int Id { get; set; }
        public LOCATION()
        {
            GroupId = (int)Globals.Groups.GRP_LOCATION;
        }

        public decimal LOCATION_LAT { get; set; }

        public decimal LOCATION_LONG { get; set; }

        public decimal LOCATION_ELEV { get; set; }

        public string LOCATION_DATE { get; set; }

        public string LOCATION_COMMENT { get; set; }
        
    }
}
