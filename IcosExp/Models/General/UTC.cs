﻿using IcosExp.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Models.General
{
    public class UTC : BaseClass
    {
        //public int Id { get; set; }

        public decimal UTC_OFFSET { get; set; }

        public string UTC_OFFSET_DATE_START { get; set; }

        public string UTC_OFFSET_COMMENT { get; set; }
        /*
        public Byte DataStatus { get; set; }

        public UInt32 InsertUserId { get; set; }

        public DateTime InsertDate { get; set; }

        public UInt32 DeleteUserId { get; set; }

        public DateTime DeletedDate { get; set; }

        public UInt32 SiteId { get; set; }*/
    }
}
