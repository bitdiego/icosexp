﻿using IcosExp.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Models.General
{
    public class TEAM : BaseClass
    {
        // public int Id { get; set; }

        public string TEAM_MEMBER_FIRSTNAME { get; set; }

        public string TEAM_MEMBER_LASTNAME { get; set; }

        public string TEAM_MEMBER_TITLE { get; set; }

        public string TEAM_MEMBER_ROLE { get; set; }

        public string TEAM_MEMBER_MAIN_EXPERT { get; set; }

        public string TEAM_MEMBER_PERC_ICOS { get; set; }

        public string TEAM_MEMBER_CONTRACT { get; set; }

        //Does not work fine....validates abc@de
        public string TEAM_MEMBER_EMAIL { get; set; }

        public string TEAM_MEMBER_ORCID { get; set; }

        public string TEAM_MEMBER_SOCIALMEDIA { get; set; }

        public string TEAM_MEMBER_PHONE { get; set; }

        public string TEAM_MEMBER_INSTITUTION { get; set; }

        public string TEAM_MEMBER_INST_STREET { get; set; }

        public string TEAM_MEMBER_INST_POSTCODE { get; set; }

        public string TEAM_MEMBER_INST_CITY { get; set; }

        public string TEAM_MEMBER_INST_COUNTRY { get; set; }

        public String TEAM_MEMBER_WORKEND { get; set; }

        public string TEAM_MEMBER_COMMENT { get; set; }

        public String TEAM_MEMBER_DATE { get; set; }
    }
}
