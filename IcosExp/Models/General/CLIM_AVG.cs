﻿using IcosExp.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Models.General
{
    public class CLIM_AVG : BaseClass
    {
        //public int Id { get; set; }
        public CLIM_AVG()
        {
            GroupId = (int)Globals.Groups.GRP_CLIM_AVG;
        }
        public decimal MAT { get; set; }

        public decimal MAP { get; set; }

        public decimal MAR { get; set; }

        public decimal MAC_YEARS { get; set; }

        public decimal MAS { get; set; }

        public string CLIMATE_KOEPPEN { get; set; }

        public string MAC_COMMENTS { get; set; }

        public string MAC_DATE { get; set; }
        /*
        public Byte DataStatus { get; set; }

        public UInt32 InsertUserId { get; set; }

        public DateTime InsertDate { get; set; }

        public UInt32 DeleteUserId { get; set; }

        public DateTime DeletedDate { get; set; }

        public UInt32 SiteId { get; set; }*/
    }
}
