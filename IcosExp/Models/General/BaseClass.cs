﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Models.General
{
    public class BaseClass
    {
        public BaseClass()
        {

        }

        public int Id { get; set; }

        public Byte DataStatus { get; set; }

        public UInt32 InsertUserId { get; set; }

        public DateTime InsertDate { get; set; }

        public UInt32 DeleteUserId { get; set; }

        public DateTime DeletedDate { get; set; }

        public UInt32 SiteId { get; set; }

        [NotMapped]
        public int GroupId { get; set; }
    }
}
