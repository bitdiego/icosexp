﻿using IcosExp.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Models
{
    public class Customer
    {
        [Required]
        [StringLength(5, MinimumLength = 5)]
        public string CustomerID { get; set; }

        [Required]
        [StringLength(40)]
        public string CompanyName { get; set; }

        [Required]
        [StringLength(40)]
        public string ContactName { get; set; }

        [Required]
        [Country]
        public string Country { get; set; }
    }
}
