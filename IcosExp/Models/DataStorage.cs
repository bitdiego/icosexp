﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Models
{
    public class DataStorage
    {
        [Key]
        public int id_datastorage { get; set; }
        public int siteID { get; set; }
        public int groupID { get; set; }
        public string groupName { get; set; }
        public string value { get; set; }
        public string DATE { get; set; }
        public string DATE_START { get; set; }
        public string DATE_END { get; set; }
        public string DATE_UNC { get; set; }
        public string COMMENT { get; set; }
        public decimal? MEAS_PRECISION { get; set; }
        public decimal? SPATIAL_VARIABILITY { get; set; }
        public string qual0 { get; set; }
        public string qual1 { get; set; }
        public string qual2 { get; set; }
        public string qual3 { get; set; }
        public string qual4 { get; set; }
        public string qual5 { get; set; }
        public string qual6 { get; set; }
        public string qual7 { get; set; }
        public string qual8 { get; set; }
        public string qual9 { get; set; }
        public string qual10 { get; set; }
        public string qual11 { get; set; }
        public string qual12 { get; set; }
        public string qual13 { get; set; }
        public string qual14 { get; set; }
        public string qual15 { get; set; }
        public string qual16 { get; set; }
        public string qual17 { get; set; }
        public string qual18 { get; set; }
        public string qual19 { get; set; }
        public string qual20 { get; set; }
        public string qual21 { get; set; }
        public string qual22 { get; set; }
        public string qual23 { get; set; }
        public string qual24 { get; set; }
        public string qual25 { get; set; }
        public string qual26 { get; set; }
        public string qual27 { get; set; }
        public string qual28 { get; set; }
        public string qual29 { get; set; }
        public string qual30 { get; set; }
        public string qual31 { get; set; }
        public string qual32 { get; set; }
        public string qual33 { get; set; }
        public string qual34 { get; set; }
        public string qual35 { get; set; }
        public string qual36 { get; set; }
        public string qual37 { get; set; }
        public string qual38 { get; set; }
        public string qual39 { get; set; }
        public string qual40 { get; set; }
        public string qual41 { get; set; }
        public string qual42 { get; set; }
        public string qual43 { get; set; }
        public string qual44 { get; set; }
        public string qual45 { get; set; }
        public string qual46 { get; set; }
        public string qual47 { get; set; }
        public string qual48 { get; set; }
        public string qual49 { get; set; }
        public string qual50 { get; set; }
        public DateTime? insertDate { get; set; }
        public int insertUserId { get; set; }
        public int? deleteUserId { get; set; }
        public DateTime? deletedDate { get; set; }
        public short? dataStatus { get; set; }
    }
}
