﻿using IcosExp.Data;
using IcosExp.Models.General;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Services
{
    public class GenericService<T> : IGenericService<T> 
        where T : BaseClass, new()
    {
        private readonly ICOSContext _context;

        public GenericService(ICOSContext ctx)
        {
            _context = ctx;
        }

        public async Task<T> GetItemAsync()
        {
            var item = await _context.Set<T>().FirstOrDefaultAsync();
            return item;
        }

        public async Task<T> GetItemValueAsync(uint siteId)
        {
            //throw new NotImplementedException();
            var item = await _context.Set<T>().Where(x => x.SiteId == siteId && x.DataStatus == 0).FirstOrDefaultAsync();
            return item;
          //  return _context.Set<T>().Find(id);
        }
        public async Task<IEnumerable<T>> GetItemValuesAsync(uint siteId)
        {
            var items = await _context.Set<T>().Where(x => x.SiteId == siteId && x.DataStatus <= 2).OrderBy(x => x.DataStatus).ToListAsync();
            return items;
        }
        public async Task<bool> SaveItemAsync(T t, uint insertUserId, uint siteId)
        {
            var item = await GetItemValueAsync(siteId);
            if (item != null)
            {
                bool b = await SetItemInvalidAsync(siteId, insertUserId, item);
                if (!b) return false;
            }
            t.DataStatus = 0;
            t.InsertUserId = insertUserId;
            t.SiteId = siteId;
            t.InsertDate = DateTime.Now;
            _context.Set<T>().Add(t);
            int res = await _context.SaveChangesAsync();
            return res > 0;
        }
        public async Task<bool> DeleteItemAsync(int? id, uint siteId, uint userId)
        {
            int res = 0;
            if (id == null) return false;
            var item = await _context.Set<T>().FirstOrDefaultAsync(x => x.Id == id && x.SiteId == siteId);
            if (item != null)
            {
                item.DataStatus = 3;
                item.DeletedDate = DateTime.Now;
                item.DeleteUserId = userId;
                res = await _context.SaveChangesAsync();
            }
            return res > 0;
        }
        public async Task<bool> UpdateItemAsync(int? id, uint siteId, uint userId, T t)
        {
            bool b = false;
            if (id == null) return false;
            var item = await _context.Set<T>().FirstOrDefaultAsync(x => x.Id == id && x.SiteId == siteId);
            if (item != null)
            {
                b = await SetItemInvalidAsync(siteId, userId, item);
            }
            if (b)
            {
                t.Id = 0;
                t.DataStatus = 0;
                t.InsertUserId = userId;
                t.SiteId = siteId;
                t.InsertDate = DateTime.Now;
                _context.Set<T>().Add(t);
                int res = await _context.SaveChangesAsync();
            }
            return b;
        }
        public async Task<bool> SetItemInvalidAsync(uint siteId, uint userId, T t)
        {
            int res = 0;
            t.DataStatus = 2;
            t.DeletedDate = DateTime.Now;
            t.DeleteUserId = userId;
            res = await _context.SaveChangesAsync();
            return res > 0;
        }
    }
}
