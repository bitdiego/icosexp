﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Services
{
    public interface IBadmListResolver
    {
        List<string> GetBadmList(int cvIndex);
    }
}
