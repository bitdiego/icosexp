﻿using IcosExp.Data;
using IcosExp.Models.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Services
{
    public class BadmListResolver : IBadmListResolver
    {
        private readonly ICOSContext _context;

        public BadmListResolver(ICOSContext context)
        {
            _context = context;
        }
        
        public List<string> GetBadmList(int cvIndex)
        {
            List<string> list = new List<string>();
            list = _context.BADMList.Where(xx => xx.cv_index == cvIndex).Select(y=>y.shortname).ToList();
            //list.AddRange(items.) List<BadmListItem> items
            return list;
        }
    }
}
