﻿using IcosExp.Data;
using IcosExp.Models.General;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Services
{
    public class ClimAvgService : IGenericService<CLIM_AVG>
    {
        ICOSContext _context;

        public ClimAvgService(ICOSContext ctx)
        {
            _context = ctx;
        }

        public async Task<CLIM_AVG> GetItemAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<CLIM_AVG> GetItemValueAsync(uint siteId)
        {
            var item = await _context.CLIM_AVG.Where(loc => loc.SiteId == siteId && loc.DataStatus == 0).FirstOrDefaultAsync();
            return item;
        }

        public async Task<IEnumerable<CLIM_AVG>> GetItemValuesAsync(uint siteId)
        {
            var locationItems = await _context.CLIM_AVG.Where(loc => loc.SiteId == siteId && loc.DataStatus <= 2).OrderBy(loc => loc.DataStatus).ToListAsync();
            return locationItems;
        }

        public async Task<bool> SaveItemAsync(CLIM_AVG location, uint insertUserId, uint siteId)
        {
            //1. find if there is another item: Location grupo cannot have multiple values!!!
            //2. if (1. is true), find present item and set datastatus to 2
            //3. then insert new item
            var locationItem = await GetItemValueAsync(siteId);
            if (locationItem != null)
            {
                bool b = await SetItemInvalidAsync(siteId, insertUserId, locationItem);
                if (!b) return false;
            }
            location.DataStatus = 0;
            location.InsertUserId = insertUserId;
            location.SiteId = siteId;
            location.InsertDate = DateTime.Now;
            _context.CLIM_AVG.Add(location);
            int res = await _context.SaveChangesAsync();
            return res > 0;
        }

        public async Task<bool> DeleteItemAsync(int? id, uint siteId, uint userId)
        {
            int res = 0;
            if (id == null) return false;
            var item = await _context.CLIM_AVG.FirstOrDefaultAsync(x => x.Id == id && x.SiteId == siteId);
            if (item != null)
            {
                item.DataStatus = 3;
                item.DeletedDate = DateTime.Now;
                item.DeleteUserId = userId;
                res = await _context.SaveChangesAsync();
            }
            return res > 0;
        }

        public async Task<bool> UpdateItemAsync(int? id, uint siteId, uint userId, CLIM_AVG location)
        {
            int res = 0;
            bool b = false;
            if (id == null) return false;
            var item = await _context.CLIM_AVG.FirstOrDefaultAsync(x => x.Id == id && x.SiteId == siteId);
            if (item != null)
            {
                item.DataStatus = 2;
                item.DeletedDate = DateTime.Now;
                item.DeleteUserId = userId;
                res = await _context.SaveChangesAsync();
            }
            if (res > 0)
            {
                location.Id = 0;
                b = await SaveItemAsync(location, userId, siteId);
            }

            return b;
        }

        public async Task<bool> SetItemInvalidAsync(uint siteId, uint userId, CLIM_AVG location)
        {
            int res = 0;
            // if (id == null) return false;
            var item = await _context.CLIM_AVG.FirstOrDefaultAsync(x => x.Id == location.Id && x.SiteId == siteId);
            if (item != null)
            {
                item.DataStatus = 2;
                item.DeletedDate = DateTime.Now;
                item.DeleteUserId = userId;
                res = await _context.SaveChangesAsync();
            }


            return res > 0;
        }
    }
}
