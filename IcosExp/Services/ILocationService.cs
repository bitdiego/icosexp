﻿using IcosExp.Models.General;
using IcosExp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Services
{
    interface ILocationService
    {
        Task<LOCATION> GetLocationValueAsync(int siteId);
        Task<LOCATIONViewModel> GetLocationViewModelAsync(int siteId);
        Task<bool> SaveLocationAsync(LOCATION location, uint insertUserId, uint siteId);
        Task<bool> DeleteLocationAsync(int? id, uint siteId, uint userId);
        Task<bool> UpdateLocationAsync(int? id, uint siteId, uint userId, LOCATION location);
        //Task<TEAM> GetTeamMemberByIdAsync(int? id);
    }
}
