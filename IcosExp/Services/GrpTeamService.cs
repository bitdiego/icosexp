﻿using AutoMapper;
using IcosExp.Data;
using IcosExp.Mappings;
using IcosExp.Models.General;
using IcosExp.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Services
{
    public class GrpTeamService : IGrpTeamService
    {
        ICOSContext _context;

        public GrpTeamService(ICOSContext ctx)
        {
            _context = ctx;
        }

        public async Task<List<TEAM>> GetGrpTeamAsync()
        {
            List<TEAM> listTeam = await _context.TEAM.Where(x => x.DataStatus == 0).ToListAsync();
            return listTeam;

        }

        public async Task<List<TEAMViewModel>> GetGrpTeamViewModelAsync()
        {
            var listTeam = await _context.TEAM.Where(x => x.DataStatus == 0).ToListAsync();
            List<TEAMViewModel> vmList = new List<TEAMViewModel>();
            return vmList;
        }

        public async Task<bool> SaveTeamMemberAsync(TEAM team, uint insertUserId, uint siteId)
        {
            team.DataStatus = 0;
            team.InsertUserId = insertUserId;
            team.SiteId = siteId;
            team.InsertDate = DateTime.Now;
            _context.TEAM.Add(team);
            int res = await _context.SaveChangesAsync();
            return res > 0;
        }

        public async Task<bool> DeleteTeamMemberAsync(int? id, uint siteId, uint userId)
        {
            int res = 0;
            if (id == null) return false;
            var item = await _context.TEAM.FirstOrDefaultAsync(x => x.Id == id && x.SiteId == siteId);
            if (item != null)
            {
                item.DataStatus = 3;
                item.DeletedDate = DateTime.Now;
                item.DeleteUserId = userId;
                res = await _context.SaveChangesAsync();
            }

            return res > 0;
        }

        public async Task<bool> UpdateTeamMemberAsync(int? id, uint siteId, uint userId, TEAM team)
        {
            int res = 0;
            bool b = false;
            if (id == null) return false;
            var item = await _context.TEAM.FirstOrDefaultAsync(x => x.Id == id && x.SiteId == siteId);
            if (item != null)
            {
                item.DataStatus = 2;
                item.DeletedDate = DateTime.Now;
                item.DeleteUserId = userId;
                res = await _context.SaveChangesAsync();
            }
            if (res > 0)
            {
                team.Id = 0;
                b = await SaveTeamMemberAsync(team, userId, siteId );
            }
            
            return b;
        }

        public async Task<List<SelectListItem>> GetTeamMemberNavPropAsync(int cvIndex)
        {
            var properties = await _context.BADMList.Where(prop => prop.cv_index==cvIndex).Select(x =>
                              new SelectListItem
                              {
                                  Value = x.shortname,
                                  Text = x.shortname
                              }).OrderBy(s=>s.Text).ToListAsync();
            return properties;
        }

        public async Task<TEAM> GetTeamMemberByIdAsync(int? _id)
        {
            var item = await _context.TEAM.Where(x => x.DataStatus == 0 && x.Id == _id).FirstOrDefaultAsync();
            return item;
        }

    }
}
