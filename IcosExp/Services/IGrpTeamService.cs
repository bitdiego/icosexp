﻿using IcosExp.Models.General;
using IcosExp.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Services
{
    public interface IGrpTeamService
    {
        Task<List<TEAM>> GetGrpTeamAsync();
        Task<List<TEAMViewModel>> GetGrpTeamViewModelAsync();
        Task<bool> SaveTeamMemberAsync(TEAM team, uint insertUserId, uint siteId);
        Task<bool> DeleteTeamMemberAsync(int? id, uint siteId, uint userId);
        Task<bool> UpdateTeamMemberAsync(int? id, uint siteId, uint userId, TEAM team);
        Task<List<SelectListItem>> GetTeamMemberNavPropAsync(int cvIndex);
        Task<TEAM> GetTeamMemberByIdAsync(int? id);
    }
}
