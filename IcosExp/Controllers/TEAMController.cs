﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IcosExp.Data;
using IcosExp.Models.General;
using IcosExp.Services;
using IcosExp.ViewModels;
using AutoMapper;
using static IcosExp.Utils.Globals;

namespace IcosExp.Controllers
{
    public class TEAMController : Controller
    {
        //private readonly ICOSContext _context;
        private readonly IGrpTeamService _service;
        private readonly IMapper _mapper;

        public TEAMController(IGrpTeamService service, IMapper mapper)
        {
            //_context = context;
            _service = service;
            _mapper = mapper;
        }

        // GET: GRP_TEAM_
        public async Task<IActionResult> Index()
        {
            //var items = await _service.GetGrpTeamViewModelAsync();
            var items = await _service.GetGrpTeamAsync();
            return View(items);
            //return View(await _context.GRP_TEAM.ToListAsync());
        }

        // GET: GRP_TEAM_/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var grpTeam = await _service.GetTeamMemberByIdAsync(id);
            if (grpTeam == null)
            {
                return NotFound();
            }

            //var user = GetUserDetails();
           
            TEAMViewModel grpViewModel = _mapper.Map<TEAMViewModel>(grpTeam);
            return View(grpViewModel);

           // return View(grpTeam);
        }/**/

        // GET: GRP_TEAM_/Create
        public async Task<IActionResult> Create()
        {
            TEAMViewModel grpTeamViewModel = new TEAMViewModel();

            grpTeamViewModel.TeamRoles = await _service.GetTeamMemberNavPropAsync((int)CvIndexes.TEAM_ROLE);
            grpTeamViewModel.TeamPerc = await _service.GetTeamMemberNavPropAsync((int)CvIndexes.TEAM_PERC);
            grpTeamViewModel.TeamExpert = await _service.GetTeamMemberNavPropAsync((int)CvIndexes.TEAM_EXP);
            grpTeamViewModel.TeamContract = await _service.GetTeamMemberNavPropAsync((int)CvIndexes.TEAM_CONTR);

            return View(grpTeamViewModel);
        }

        // POST: GRP_TEAM_/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,TEAM_MEMBER_FIRSTNAME,TEAM_MEMBER_LASTNAME,TEAM_MEMBER_TITLE,TEAM_MEMBER_ROLE,TEAM_MEMBER_MAIN_EXPERT,TEAM_MEMBER_PERC_ICOS,TEAM_MEMBER_CONTRACT,TEAM_MEMBER_EMAIL,TEAM_MEMBER_ORCID,TEAM_MEMBER_SOCIALMEDIA,TEAM_MEMBER_PHONE,TEAM_MEMBER_INSTITUTION,TEAM_MEMBER_INST_STREET,TEAM_MEMBER_INST_POSTCODE,TEAM_MEMBER_INST_CITY,TEAM_MEMBER_INST_COUNTRY,TEAM_MEMBER_WORKEND,TEAM_MEMBER_COMMENT,TEAM_MEMBER_DATE")] TEAMViewModel grpTeamVM)
        {
            if (ModelState.IsValid)
            {
                //_context.Add(grpTeam);
                //await _context.SaveChangesAsync();
                TEAM grpTeam = _mapper.Map<TEAM>(grpTeamVM); // new GRP_TEAM();
                /*
                {
                    TEAM_MEMBER_FIRSTNAME = grpTeamVM.TEAM_MEMBER_FIRSTNAME,
                    TEAM_MEMBER_LASTNAME=grpTeamVM.TEAM_MEMBER_LASTNAME,
                    TEAM_MEMBER_TITLE=grpTeamVM.TEAM_MEMBER_TITLE,
                    TEAM_MEMBER_CONTRACT=grpTeamVM.TEAM_MEMBER_CONTRACT,
                    TEAM_MEMBER_COMMENT=grpTeamVM.TEAM_MEMBER_COMMENT,
                    TEAM_MEMBER_DATE=grpTeamVM.TEAM_MEMBER_DATE,
                    TEAM_MEMBER_EMAIL=grpTeamVM.TEAM_MEMBER_EMAIL,
                    TEAM_MEMBER_INSTITUTION=grpTeamVM.TEAM_MEMBER_INSTITUTION,
                    TEAM_MEMBER_INST_CITY=grpTeamVM.TEAM_MEMBER_INST_CITY,
                    TEAM_MEMBER_INST_COUNTRY=grpTeamVM.TEAM_MEMBER_INST_COUNTRY,
                    TEAM_MEMBER_INST_POSTCODE=grpTeamVM.TEAM_MEMBER_INST_POSTCODE,
                    TEAM_MEMBER_INST_STREET=grpTeamVM.TEAM_MEMBER_INST_STREET,
                    TEAM_MEMBER_MAIN_EXPERT=grpTeamVM.TEAM_MEMBER_MAIN_EXPERT,
                    TEAM_MEMBER_ORCID = grpTeamVM.TEAM_MEMBER_ORCID,
                    TEAM_MEMBER_PERC_ICOS=grpTeamVM.TEAM_MEMBER_PERC_ICOS,
                    TEAM_MEMBER_PHONE=grpTeamVM.TEAM_MEMBER_PHONE,
                    TEAM_MEMBER_ROLE=grpTeamVM.TEAM_MEMBER_ROLE,
                    TEAM_MEMBER_SOCIALMEDIA=grpTeamVM.TEAM_MEMBER_SOCIALMEDIA,
                    TEAM_MEMBER_WORKEND=grpTeamVM.TEAM_MEMBER_WORKEND,
                    DataStatus=0,
                    InsertDate=DateTime.Now,
                    InsertUserId=95,
                    SiteId=70
                };*/

                bool res = await _service.SaveTeamMemberAsync(grpTeam, 95, 70);
                return RedirectToAction(nameof(Index));
            }

            return View(grpTeamVM);
        }

        // GET: GRP_TEAM_/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetTeamMemberByIdAsync(id);
            if (item == null)
            {
                return NotFound();
            }

            TEAMViewModel grpViewModel = _mapper.Map<TEAMViewModel>(item);
            grpViewModel.TeamRoles = await _service.GetTeamMemberNavPropAsync((int)CvIndexes.TEAM_ROLE);
            grpViewModel.TeamPerc = await _service.GetTeamMemberNavPropAsync((int)CvIndexes.TEAM_PERC);
            grpViewModel.TeamExpert = await _service.GetTeamMemberNavPropAsync((int)CvIndexes.TEAM_EXP);
            grpViewModel.TeamContract = await _service.GetTeamMemberNavPropAsync((int)CvIndexes.TEAM_CONTR);

           // grpViewModel.TeamRoles.S

            return View(grpViewModel);

           // return View(item);
        }

        // POST: GRP_TEAM_/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,TEAM_MEMBER_FIRSTNAME,TEAM_MEMBER_LASTNAME,TEAM_MEMBER_TITLE,TEAM_MEMBER_ROLE,TEAM_MEMBER_MAIN_EXPERT,TEAM_MEMBER_PERC_ICOS,TEAM_MEMBER_CONTRACT,TEAM_MEMBER_EMAIL,TEAM_MEMBER_ORCID,TEAM_MEMBER_SOCIALMEDIA,TEAM_MEMBER_PHONE,TEAM_MEMBER_INSTITUTION,TEAM_MEMBER_INST_STREET,TEAM_MEMBER_INST_POSTCODE,TEAM_MEMBER_INST_CITY,TEAM_MEMBER_INST_COUNTRY,TEAM_MEMBER_WORKEND,TEAM_MEMBER_COMMENT,TEAM_MEMBER_DATE,DataStatus,InsertUserId,InsertDate,DeleteUserId,DeletedDate,SiteId")] TEAMViewModel vm)
        {
            if (id != vm.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    TEAM grpTeam = _mapper.Map<TEAM>(vm); // new GRP_TEAM();
                    grpTeam.DataStatus = 0;
                    grpTeam.InsertUserId = 95;
                    grpTeam.SiteId = 70;
                    await _service.UpdateTeamMemberAsync(id, 70, 95, grpTeam);
                    //_context.Update(gRP_TEAM);
                    //await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    /*if (!GRP_TEAMExists(gRP_TEAM.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }*/
                }
                return RedirectToAction(nameof(Index));
            }
            return View(vm);
        }

        // GET: GRP_TEAM_/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var res = await _service.DeleteTeamMemberAsync(id, 70, 95);
            if (res == false)
            {
                return NotFound();
            }

            return RedirectToAction("Index");
        }

        // POST: GRP_TEAM_/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? id)
        {
            /* var gRP_TEAM = await _context.GRP_TEAM.FindAsync(id);
             _context.GRP_TEAM.Remove(gRP_TEAM);
             await _context.SaveChangesAsync();
             return RedirectToAction(nameof(Index));*/
            if (id == null)
            {
                return NotFound();
            }

            var res = await _service.DeleteTeamMemberAsync(id, 70, 95);
            if (res == false)
            {
                return NotFound();
            }

            return RedirectToAction("Index");
        }
        /*
        private bool GRP_TEAMExists(int id)
        {
            return _context.GRP_TEAM.Any(e => e.Id == id);
        }*/
    }
}
