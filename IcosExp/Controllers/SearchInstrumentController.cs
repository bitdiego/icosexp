﻿using IcosExp.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchInstrumentController : ControllerBase
    {
        private ICOSContext db;

        public SearchInstrumentController(ICOSContext _db)
        {
            db = _db;
        }

        [Produces("application/json")]
        [HttpGet("search")]    
        public async Task<IActionResult> Search()
        {
            try
            {
                string term = HttpContext.Request.Query["term"].ToString();
                string extra = HttpContext.Request.Query["Extra"].ToString();
                var names = await db.BADMList.Where(cv => cv.cv_index == int.Parse(extra) && cv.shortname.Contains(term))
                                             .Select(n => n.shortname).ToListAsync();
                return Ok(names);
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
