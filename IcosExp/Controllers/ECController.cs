﻿using IcosExp.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosExp.Controllers
{
    public class ECController : Controller
    {
        // GET: ECController
        public ActionResult Index()
        {
            List<ECViewModel> ecList = new List<ECViewModel>();
            return View(ecList);
        }

        // GET: ECController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ECController/Create
        public ActionResult Create()
        {
            ECViewModel ecVM = new ECViewModel();

            return View(ecVM);
        }

        // POST: ECController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ECController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ECController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ECController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ECController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
