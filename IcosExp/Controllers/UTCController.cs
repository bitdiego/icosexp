﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IcosExp.Data;
using IcosExp.Models.General;

namespace IcosExp.Controllers
{
    public class UTCController : Controller
    {
        private readonly ICOSContext _context;

        public UTCController(ICOSContext context)
        {
            _context = context;
        }

        // GET: UTC
        public async Task<IActionResult> Index()
        {
            return View(await _context.UTC.ToListAsync());
        }

        // GET: UTC/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var uTC = await _context.UTC
                .FirstOrDefaultAsync(m => m.Id == id);
            if (uTC == null)
            {
                return NotFound();
            }

            return View(uTC);
        }

        // GET: UTC/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: UTC/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,UTC_OFFSET,UTC_OFFSET_DATE_START,UTC_OFFSET_COMMENT,DataStatus,InsertUserId,InsertDate,DeleteUserId,DeletedDate,SiteId")] UTC uTC)
        {
            if (ModelState.IsValid)
            {
                _context.Add(uTC);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(uTC);
        }

        // GET: UTC/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var uTC = await _context.UTC.FindAsync(id);
            if (uTC == null)
            {
                return NotFound();
            }
            return View(uTC);
        }

        // POST: UTC/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UTC_OFFSET,UTC_OFFSET_DATE_START,UTC_OFFSET_COMMENT,DataStatus,InsertUserId,InsertDate,DeleteUserId,DeletedDate,SiteId")] UTC uTC)
        {
            if (id != uTC.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(uTC);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UTCExists(uTC.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(uTC);
        }

        // GET: UTC/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var uTC = await _context.UTC
                .FirstOrDefaultAsync(m => m.Id == id);
            if (uTC == null)
            {
                return NotFound();
            }

            return View(uTC);
        }

        // POST: UTC/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var uTC = await _context.UTC.FindAsync(id);
            _context.UTC.Remove(uTC);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UTCExists(int id)
        {
            return _context.UTC.Any(e => e.Id == id);
        }
    }
}
