﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IcosExp.Data;
using IcosExp.ViewModels;
using AutoMapper;
using IcosExp.Services;
using IcosExp.Models.General;

namespace IcosExp.Controllers
{
    public class CLIM_AVGController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IGenericService<CLIM_AVG> _service;

        public CLIM_AVGController(IGenericService<CLIM_AVG> service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: CLIM_AVG
        public async Task<IActionResult> Index()
        {
            var climate = await _service.GetItemValueAsync(70);
            if (climate != null)
            { 
                var climateVM = _mapper.Map<CLIM_AVGViewModel>(climate);
                return View(climateVM);
            }
            /*   */
            return View();
        }

        // GET: CLIM_AVG/Details/5
        public async Task<IActionResult> Details(int? id)
        {
             if (id == null)
             {
                 return NotFound();
             }

            var item = await _service.GetItemAsync();
            if (item != null)
            {
                var climateVM = _mapper.Map<CLIM_AVGViewModel>(item);
                return View(climateVM);
            }
           
             else
             {
                 return NotFound();
             }
        }

        // GET: CLIM_AVG/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CLIM_AVG/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,MAT,MAP,MAR,MAC_YEARS,MAS,CLIMATE_KOEPPEN,MAC_COMMENTS,MAC_DATE,DataStatus")] CLIM_AVGViewModel climateVM)
        {
            if (ModelState.IsValid)
            {
                CLIM_AVG climate = _mapper.Map< CLIM_AVG>(climateVM);
                bool res = await _service.SaveItemAsync(climate, 95, 70);
                if(res)
                    return RedirectToAction(nameof(Index));
                else return View(climateVM);

            }
            /**/
            return View();
        }

        // GET: CLIM_AVG/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var item = await _service.GetItemValueAsync(70);
            if (item != null)
            {
                var climateVM = _mapper.Map<CLIM_AVGViewModel>(item);
                return View(climateVM);
            }

            else
            {
                return NotFound();
            }
        }

        // POST: CLIM_AVG/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,MAT,MAP,MAR,MAC_YEARS,MAS,CLIMATE_KOEPPEN,MAC_COMMENTS,MAC_DATE,DataStatus")] CLIM_AVGViewModel climateVM)
        {
            if (id != climateVM.Id)
            {
                return NotFound();
            }
            
            if (ModelState.IsValid)
            {
                try
                {
                    var climate = _mapper.Map<CLIM_AVG>(climateVM);
                    bool b = await _service.UpdateItemAsync(id, 70, 95, climate);
                    if (b)
                        return View("Index", _mapper.Map<CLIM_AVGViewModel>(climate));
                    else
                        return NotFound();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CLIM_AVGViewModelExists(climateVM.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(climateVM);

//            return View();
        }

        // GET: CLIM_AVG/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            /*
                        var cLIM_AVGViewModel = await _context.CLIM_AVGViewModel
                            .FirstOrDefaultAsync(m => m.Id == id);
                        if (cLIM_AVGViewModel == null)
                        {
                            return NotFound();
                        }

                        return View(cLIM_AVGViewModel);*/
            return View();
        }

        // POST: CLIM_AVG/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            /*  var cLIM_AVGViewModel = await _context.CLIM_AVGViewModel.FindAsync(id);
              _context.CLIM_AVGViewModel.Remove(cLIM_AVGViewModel);
              await _context.SaveChangesAsync();
              return RedirectToAction(nameof(Index));*/
            return View();
        }

        private bool CLIM_AVGViewModelExists(int id)
        {
            // return _context.CLIM_AVGViewModel.Any(e => e.Id == id);
            return false;
        }
    }
}
