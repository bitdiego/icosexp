﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IcosExp.Data;
using IcosExp.ViewModels;
using IcosExp.Services;
using IcosExp.Models.General;
using AutoMapper;
/*********/


namespace IcosExp.Controllers
{
    public class LOCATIONController : Controller
    {
        private readonly IGenericService<LOCATION> _service;
        private readonly IMapper _mapper;

        public LOCATIONController(IGenericService<LOCATION> service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: LOCATION
        public async Task<IActionResult> Index()
        {
            var item = await _service.GetItemValuesAsync(70);
            if (item != null)
            {
                List<LOCATIONViewModel> lvmList = new List<LOCATIONViewModel>();
                foreach(var location in item)
                {
                    var locationVM = _mapper.Map<LOCATIONViewModel>(location);
                    lvmList.Add(locationVM);
                }
                
                return View(lvmList);
            }
            return View();
        }

        // GET: LOCATION/Details/5
       /* public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lOCATIONViewModel = await _context.LOCATIONViewModel
                .FirstOrDefaultAsync(m => m.Id == id);
            if (lOCATIONViewModel == null)
            {
                return NotFound();
            }

            return View(lOCATIONViewModel);
        }*/

        // GET: LOCATION/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: LOCATION/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,LOCATION_LAT,LOCATION_LONG,LOCATION_ELEV,LOCATION_DATE,LOCATION_COMMENT")] LOCATIONViewModel locationVM)
        {
            if (ModelState.IsValid)
            {
                LOCATION location = _mapper.Map<LOCATION>(locationVM);

                /*
                Validator validator = new Validator();
                int bb = validator.ValidateLocation(location);
                */

                bool res = await _service.SaveItemAsync(location, 95, 70);
                return RedirectToAction(nameof(Index));
            }
            return View(locationVM);
        }

        // GET: LOCATION/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var location = await _service.GetItemValueAsync(70);
            if (location == null)
            {
                return NotFound();
            }
            var locationVM = _mapper.Map<LOCATIONViewModel>(location);
            return View(locationVM);
        }

        // POST: LOCATION/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,LOCATION_LAT,LOCATION_LONG,LOCATION_ELEV,LOCATION_DATE,LOCATION_COMMENT")] LOCATIONViewModel locationViewModel)
        {
            if (id != locationViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var locationModel = _mapper.Map<LOCATION>(locationViewModel);
                    bool res = await _service.UpdateItemAsync(id, 70, 95, locationModel);
                    if(res)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    /*if (!LOCATIONViewModelExists(lOCATIONViewModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }*/
                    return RedirectToAction(nameof(Index)); //set error message...
                }
                
            }
            return View(locationViewModel);
        }

        // GET: LOCATION/Delete/5
      /*  public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lOCATIONViewModel = await _context.LOCATIONViewModel
                .FirstOrDefaultAsync(m => m.Id == id);
            if (lOCATIONViewModel == null)
            {
                return NotFound();
            }

            return View(lOCATIONViewModel);
        }
        */
        // POST: LOCATION/Delete/5
       /* [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var lOCATIONViewModel = await _context.LOCATIONViewModel.FindAsync(id);
            _context.LOCATIONViewModel.Remove(lOCATIONViewModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LOCATIONViewModelExists(int id)
        {
            return _context.LOCATIONViewModel.Any(e => e.Id == id);
        }*/
    }
}
